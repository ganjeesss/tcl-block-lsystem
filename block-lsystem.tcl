#!/usr/bin/tclsh

# block replacement l-system with netppm output
# and animated gifs via imagemagick

# replace one line
proc replace_line { b_line w h dict } {
    
    set o_lines [list]
    set size [expr {$w*$h}]
    
    # create blank strings
    for {set y 0} {$y < $h} {incr y} {
        lappend o_lines ""
    }
    
    # replace each char in input
    foreach c_char [split $b_line ""] {
    
        # the current substitution (out)
        set c_subs [dict get $dict $c_char]
        
        for {set j 0} {$j < $size} {incr j} {
            # current "row"
            set y [expr {int($j / $w)}]
            lset o_lines $y "[lindex $o_lines $y][string index $c_subs $j]"
        }
    
    }
    
    # build output string
    set o_text ""
    for {set y 0} {$y < $h} {incr y} {
        set o_text "${o_text}\n[lindex $o_lines $y]"
    }
    
    return [string trim $o_text]

}

# replace every line
proc replace_all { b_text w h dict } {
    
    set lines [split $b_text]
    set o_text ""
    foreach line $lines {
        set o_text "${o_text}\n[replace_line $line $w $h $dict]"
    }
    
    return [string trim $o_text]
    
}

# convert to ppm
proc to_ppm { b_text w h colours_dict } {

    set o_text ""
    
    # calculate text width and height
    set width [string length [lindex [split $b_text] 0]]
    set height [llength [split $b_text]]
    
    # remove whitespace
    regsub -all {\s} $b_text {} b_text
    
    # ppm "header"
    append o_text "P3\n"
    append o_text "$width $height\n"
    append o_text "255\n"
    
    # put the pixels
    foreach c_char [split $b_text ""] {
        if [dict exists $colours_dict $c_char] {
            append o_text "[dict get $colours_dict $c_char]\n"
        } else {
            append o_text "0 0 0\n"
        }
    }
    
    return $o_text

}

# iterate x times
proc iterations { b_text w h lsystem_dict iterations } {

    # iterate
    for {set i 0} {$i < $iterations} {incr i} {
        set b_text [replace_all $b_text $w $h $lsystem_dict]
    }
    
    return $b_text
    
}

# create ppm (convenience/backward compatibility)
proc ppm_lsystem { b_text w h lsystem_dict iterations colours_dict } {
    
    set b_text [iterations $b_text $w $h $lsystem_dict $iterations]
    return [to_ppm $b_text $w $h $colours_dict]
    
}

# create animation (requires imagemagick to be installed & in path)
proc gif_lsystem { b_text w h lsystem_dict iterations colours_dict delay outfile } {
    
    # iterate
    for {set i 0} {$i < $iterations} {incr i} {
    
        # write ppm
        set ofh [open "TEMP_[format %03d $i].ppm" w]
        puts $ofh [to_ppm $b_text $w $h $colours_dict]
        close $ofh
    
        set b_text [replace_all $b_text $w $h $lsystem_dict]
        
    }
    
    # write final ppm
    set ofh [open "TEMP_[format %03d $i].ppm" w]
    puts $ofh [to_ppm $b_text $w $h $colours_dict]
    close $ofh
    
    # calculate final width and height
    set width [string length [lindex [split $b_text] 0]]
    set height [llength [split $b_text]]
    
    # resize
    foreach f [glob TEMP_*.ppm] {
        regsub {\.ppm$} $f {.gif} g
        exec -ignorestderr -- convert $f -filter Point -resize ${width}x${height} $g
        file delete $f
    }

    # convert to gif
    exec -ignorestderr -- convert -delay $delay -loop 0 TEMP_*.gif $outfile
    
    # remove temp gifs
    foreach f [glob TEMP_*.gif] {
        file delete $f
    }
    
}   


