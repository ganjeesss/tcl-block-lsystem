#!/usr/bin/tclsh

source "../block-lsystem.tcl"

set lsystem [dict create \
    A AAAABAAAA \
    B BBBBBBBBB \
]

set colours [dict create \
    A "000 000 000" \
    B "255 255 255" \
]

gif_lsystem "A" 3 3 $lsystem 6 $colours 100 "example.gif"
