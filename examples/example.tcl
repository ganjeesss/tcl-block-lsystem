#!/usr/bin/tclsh

source "../block-lsystem.tcl"

set lsystem [dict create \
    A BAAA \
    B BCBB \
    C CCDC \
    D DDDA \
]

set colours [dict create \
    A "55 73 73"    \
    B "91 123 122"  \
    C "153 144 129" \
    D "231 210 191" \
]

set ofh [open "example.ppm" w]
puts $ofh [ppm_lsystem "A" 2 2 $lsystem 10 $colours]
close $ofh