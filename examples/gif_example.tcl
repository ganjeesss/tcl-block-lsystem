#!/usr/bin/tclsh

source "../block-lsystem.tcl"

set lsystem [dict create \
    A BAAA \
    B BBBB \
]

set colours [dict create \
    A "000 000 000" \
    B "255 255 255" \
]

gif_lsystem "A" 2 2 $lsystem 10 $colours 100 "example.gif"
