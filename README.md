# block lsystem

its a 2d l-system that renders to netppm images. a 2d l-system is something like this:

    A => AB 
         CD
         
    B => AA 
         AA
         
    C => BC
         DA
         
    D => DD 
         DD
    
    1.
        A
    
    2.
        AB
        CD
        
    3.  
        ABAA
        CDAA
        BCDD
        DADD

etc, and in the netppm file it will be coloured pixels rather than letters. you can open the netppm file in something like irfanview or other image software. look in example.tcl for an example

if you have imagemagick installed you can also produce animated gifs of the fractal growing (see gif_example.tcl)